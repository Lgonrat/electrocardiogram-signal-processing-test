function testECGPreprocessing()

    % Load ECG data for testing
    load('ecgConditioningExample.mat');
    fs=1000;
    % Test 1: Assessing data type compability
    assert(isa(ecg, 'double'), 'Error: Input data type should be double.');

    % Test 2: Verify functionality of preprocessing functions
    disp('Running preprocessing functions...');

    % Test removeRecordingSpikes
    [pks, locs] = removeRecordingSpikes(ecg);

    % Test medianFiltering
    t = (1:length(ecg)) / fs;
    ecg_medfilt = medianFiltering(ecg, t);

    % Test detrendECG
    ecg_detrend = detrendECG(ecg_medfilt, t);

    % Test lowPassFiltering
    ecg_LowPass = lowPassFiltering(ecg_detrend, t, fs);

    % Test bandPassFiltering
    ecg_bandpass = bandPassFiltering(ecg_detrend, t, fs);

    % Test baselineWanderRemoval
    ecg_FIR = baselineWanderRemoval(ecg_detrend, t, fs);

    disp('Preprocessing functions completed.');

    % Test 3: Validate expected results
    assert(all(size(pks) == size(locs)), 'Error: Size mismatch in peak detection.');
    assert(all(size(ecg) == size(ecg_medfilt)), 'Error: Size mismatch in median filtering.');
    assert(all(size(ecg_detrend) == size(ecg_LowPass)), 'Error: Size mismatch in low-pass filtering.');
    assert(all(size(ecg_detrend) == size(ecg_bandpass)), 'Error: Size mismatch in bandpass filtering.');
    assert(all(size(ecg_detrend) == size(ecg_FIR)), 'Error: Size mismatch in FIR filtering.');

    % Display results and information
    disp('Test results:');

    % Plot original and preprocessed signals for visual inspection
    figure;
    subplot(2, 3, 1); plotECGSignal(t, ecg, 'Original ECG');
    subplot(2, 3, 2); plotECGSignal(t, ecg_medfilt, 'Remove Spikes');
    subplot(2, 3, 3); plotECGSignal(t, ecg_detrend, 'Remove Linear Trends');
    subplot(2, 3, 4); plotECGSignal(t, ecg_LowPass, 'Low-pass Filtered');
    subplot(2, 3, 5); plotECGSignal(t, ecg_bandpass, 'Powerline interference filtering');
    subplot(2, 3, 6); plotECGSignal(t, ecg_FIR, 'Baseline wander and offset removal');

end

function plotECGSignal(t, ecg, titleText)
    plot(t, ecg);
    xlabel('Time (s)');
    title(titleText);
    grid on;
end

function [pks, locs] = removeRecordingSpikes(ecg)
    % Recording spikes (artifacts of great amplitude) removal using findpeaks
    for k =1:size(ecg,2)
        [pks,locs]=findpeaks(ecg(:,k),'MinPeakDistance',10000);
    end
end

function ecg_medfilt = medianFiltering(ecg, t)
    % Median filtering
    ecg_medfilt = medfilt1(ecg, 9);
%     figure;
%     plot(t, ecg);
%     hold on;
%     plot(t, ecg_medfilt, 'r');
%     xlabel('Time(s)');
%     title('ECG signal with Median Filtering');
%     legend('ECG original', 'ECG Median Filtered');
%     hold off;
%     grid on;
end

function ecg_detrend = detrendECG(ecg_medfilt, t)
    % Detrend ECG signal
    ecg_detrend = detrend(ecg_medfilt);   % Remove linear trends
%     figure;
%     plot(t, ecg_detrend, 'r');
%     title('ECG Before Detrending');
%     xlabel('Time(s)');
%     grid on;
end

function ecg_LowPass = lowPassFiltering(ecg_detrend, t, fs)
    % Low-pass filtering
    fc_L = 2;
    ecg_LowPass = lowpass(ecg_detrend, fc_L, fs);
%     figure;
%     plot(t, ecg_detrend);
%     hold on;
%     plot(t, ecg_LowPass, 'r');
%     xlabel('Time(s)');
%     legend('ECG original', 'ECG Filtered');
%     title('Low Pass Filtering');
%     hold off;
end

function ecg_bandpass = bandPassFiltering(ecg_detrend, t, fs)
    % Powerline interference filtering (Band Pass)
    fpass = [50 200]/(fs/2);
    ecg_bandpass = bandpass(ecg_detrend, fpass, fs);
%     figure;
%     subplot(2,1,1);
%     plot(t, ecg_bandpass);
%     xlabel('Time(s)');
%     title('ECG without interferences');
%     subplot(2,1,2);
%     plot(t, ecg_detrend);
%     hold on;
%     plot(t, ecg_bandpass, '-r');
%     hold off;
%     xlabel('Time(s)');
%     title('Powerline Interferance Filtering');
%     legend('ECG Original','ECG BandPass Filtered');
end

function ecg_FIR = baselineWanderRemoval(ecg_detrend, t, fs)
    % Baseline wander and offset removal (FIR Filter)
    d = designfilt('lowpassfir', 'FilterOrder', 70, ...
                   'CutoffFrequency', 75, 'SampleRate', fs);
    ecg_FIR = filter(d, ecg_detrend);
%     figure;
%     plot(t, ecg_detrend);
%     hold on;
%     plot(t, ecg_FIR, '-r');
%     hold off;
%     title ('Baseline wander and offset removal');
%     xlabel ('Time (s)');
%     legend('Original Signal', 'Filtered Signal');
end

