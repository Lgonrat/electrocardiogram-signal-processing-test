function preprocess_ecg()
    % Load ECG data
    load('ecgConditioningExample.mat');
    
    % Sampling frequency
    fs = 1000;    % [Hz]
    Ts = 1/fs;    % [s]
    N = length(ecg);
    t = (1:1:N)*Ts;
    
    % Plot original ECG signal
    plotECGSignal(t, ecg);

    % Check if the channel is connected
    checkChannelConnection(ecg);

    % Remove recording spikes
    [pks, locs] = removeRecordingSpikes(ecg);

    % Plot spikes
    plotSpikes(ecg, t, locs, pks);

    % Median filtering
    ecg_medfilt = medianFiltering(ecg, t);

    % Plot original and median-filtered ECG
    plotOriginalAndFilteredECG(t, ecg, ecg_medfilt);

    % Plot ECG without spikes
    plotECGWithoutSpikes(t, ecg_medfilt);

    % Detrend ECG signal
    ecg_detrend = detrendECG(ecg_medfilt, t);

    % Low-pass filtering
    ecg_LowPass = lowPassFiltering(ecg_detrend, t, fs);

    % Plot original and filtered ECG after low-pass filtering
    plotOriginalAndFilteredECG(t, ecg_detrend, ecg_LowPass);

    % Powerline interference filtering (Band Pass)
    ecg_bandpass = bandPassFiltering(ecg_detrend, t, fs);

    % Plot original and bandpass-filtered ECG
    plotOriginalAndFilteredECG(t, ecg_detrend, ecg_bandpass);

    % Baseline wander and offset removal (FIR Filter)
    ecg_FIR = baselineWanderRemoval(ecg_detrend, t, fs);

    % Plot original and FIR-filtered ECG
    plotOriginalAndFilteredECG(t, ecg_detrend, ecg_FIR);
end

function plotECGSignal(t, ecg)
    figure
    plot(t, ecg, 'b');
    xlabel('Time(s)');
    title('ECG Dominance Over Time');
    hold off;
end

function checkChannelConnection(ecg)
    % Detect if a given channel is not connected
    if isempty(ecg)
        error('Channel is not connected');
    end
end

function [pks, locs] = removeRecordingSpikes(ecg)
    % Recording spikes (artifacts of great amplitude) removal using findpeaks
    for k =1:size(ecg,2)
        [pks,locs]=findpeaks(ecg(:,k),'MinPeakDistance',10000);
    end
end

function plotSpikes(ecg, t, locs, pks)
    figure
    hold on;
    plot(ecg);
    plot(locs, pks, 'vr', 'MarkerFaceColor', 'r');
    xlabel('Time(s)');
    title('Spikes');
    hold off;
end

function ecg_medfilt = medianFiltering(ecg, t)
    % Median filtering --> reduce noise
    ecg_medfilt = medfilt1(ecg, 9);
    figure
    hold on;
    plot(t, ecg);
    plot(t, ecg_medfilt, 'r');
    xlabel('Time(s)');
    title('ECG signal with Median Filtering');
    legend('ECG original', 'ECG Median Filtered');
    hold off;
    grid on;
end

function plotOriginalAndFilteredECG(t, ecg, filteredECG)
    figure;
    plot(t, ecg);
    hold on;
    plot(t, filteredECG, 'r');
    xlabel('Time(s)');
    title('Original and Filtered ECG');
    legend('ECG original', 'Filtered ECG');
    hold off;
end

function plotECGWithoutSpikes(t, ecg_medfilt)
    figure;
    plot(t, ecg_medfilt);
    xlabel('Time(s)');
    title('Recording Spikes Removal');
    hold off;
end

function ecg_detrend = detrendECG(ecg_medfilt, t)
    % Detrend ECG signal
    ecg_detrend = detrend(ecg_medfilt);   % Remove linear trends
    figure;
    plot(t, ecg_detrend, 'r');
    title('ECG Before Detrending');
    xlabel('Time(s)');
    grid on;
end

function ecg_LowPass = lowPassFiltering(ecg_detrend, t, fs)
    % Low-pass filtering --> remove high frequency
    fc_L = 2;
    ecg_LowPass = lowpass(ecg_detrend, fc_L, fs);
    figure;
    plot(t, ecg_detrend);
    hold on;
    plot(t, ecg_LowPass, 'r');
    xlabel('Time(s)');
    legend('ECG original', 'ECG Filtered');
    title('Low Pass Filtering');
    hold off;
end

function ecg_bandpass = bandPassFiltering(ecg_detrend, t, fs)
    % Powerline interference filtering (Band Pass) --> remove low frequency and high frequency
    fpass = [50 200]/(fs/2);
    ecg_bandpass = bandpass(ecg_detrend, fpass, fs);
    figure;
    subplot(2,1,1);
    plot(t, ecg_bandpass);
    xlabel('Time(s)');
    title('ECG without interferences');
    subplot(2,1,2);
    plot(t, ecg_detrend);
    hold on;
    plot(t, ecg_bandpass, '-r');
    hold off;
    xlabel('Time(s)');
    title('Powerline Interferance Filtering');
    legend('ECG Original','ECG BandPass Filtered');
end

function ecg_FIR = baselineWanderRemoval(ecg_detrend, t, fs)
    % Baseline wander and offset removal (FIR Filter) --> helps to preserve the characteristics of a filtered time waveform exactly where they occur in the unfiltered signal
    d = designfilt('lowpassfir', 'FilterOrder', 70, ...
                   'CutoffFrequency', 75, 'SampleRate', fs);
    ecg_FIR = filter(d, ecg_detrend);
    figure;
    plot(t, ecg_detrend);
    hold on;
    plot(t, ecg_FIR, '-r');
    hold off;
    title ('Baseline wander and offset removal');
    xlabel ('Time (s)');
    legend('Original Signal', 'Filtered Signal');
end
